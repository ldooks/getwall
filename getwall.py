import sys
import os
import requests

def sizeof_fmt(num, suffix='B'):
  """http://stackoverflow.com/a/1094933."""
  for unit in ['', 'Ki', 'Mi', 'Gi', 'Ti', 'Pi', 'Ei', 'Zi']:
      if abs(num) < 1024.0:
          return "%3.1f%s%s" % (num, unit, suffix)
      num /= 1024.0
  return "%.1f%s%s" % (num, 'Yi', suffix)

try:
  link = sys.argv[1]
except IndexError:
  print 'Looks like you forgot a url'
  sys.exit(1)

image = requests.get(link, allow_redirects=True, stream=True)
image_name = link.split('/')[-1]


if 'unsplash' in link:
  # Optionally, get the image name from the headers
  # image_name = image.headers['Content-Disposition'].split('=')[1].replace('"', '')
  image_name = link.split('/')[4] + '.jpeg'

target = "%s/Dropbox/Photos/wallpaper/%s" % (os.environ['HOME'], image_name)


if os.path.exists(target):
    print "Not downloading duplicate %s" % image_name
else:
    print "Downloading %s, size: %s" % (image_name, sizeof_fmt(int(image.headers['Content-length'])))
    open(target, 'wb').write(image.content)
